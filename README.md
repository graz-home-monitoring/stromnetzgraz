# Java library for Stromnetz Graz API

This library functions as middle man in my greater (upcoming) project for monitoring my apartment in Graz.
The readings are fetched through the [Stromnetz Graz website](https://webportal.stromnetz-graz.at/) and then printed to
the console (see example below).

## Example

```java
public static void main(String[] args) throws JsonProcessingException {

    var email = "";
    var password = "";

    StromNetz sn = new StromNetz(email, password);
    ArrayList<Installation> installations = sn.listInstallations();


    for (Installation i : installations) {
        for (Meter m : i.getMeterPoints()) {
            System.out.println(sn.fetch_consumption_data(m));

        }
    }
}
```

`email`, which is used to access the web-portal  
`password`, which is used to access the web-portal  
`Installation` is the the housing complex, which holds multiple electrity-meters  
`Meter` a single smart-meter

Thanks to [dreautall's](https://github.com/dreautall/stromnetzgraz/), who already implemented the same library in
python!