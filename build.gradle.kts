plugins {
    id("java")
}

group = "org.sngraz"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
    implementation("org.apache.httpcomponents.client5:httpclient5:5.3-alpha1")
    implementation("com.auth0:java-jwt:4.4.0")
    implementation("com.fasterxml.jackson.core:jackson-core:2.15.2")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.10.0")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}