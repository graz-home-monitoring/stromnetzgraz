package org.sngraz;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;

public class StromNetz {

    private static final String API_ENDPOINT = "https://webportal.stromnetz-graz.at/api/";

    private final String email;
    private final String password;

    private HttpClient httpClient;
    private HttpRequest httpRequest = null;

    private final int TIMEOUT = 10;
    private String jwt;
    private ObjectMapper mapper;
    private TypeFactory typeFactory;

    public StromNetz(String email, String password) {
        this.email = email;
        this.password = password;

        this.httpClient = HttpClient.newHttpClient();
        this.mapper = new ObjectMapper();
        this.typeFactory = this.mapper.getTypeFactory();
    }

    /**
     * Connects to the StromNetz-Graz APIs and performs HTTP POST requests
     *
     * @param path    Path to requested resource
     * @param payload Information for the API e.g. login credentials
     * @return String which contains response from API (json)
     */
    public String query(String path, String payload) {

        //check if still authenticated
        ArrayList<String> header = new ArrayList<>();

        header.add("Content-Type");
        header.add("application/json");

        if (this.ok()) {
            header.add("Authorization");
            header.add("Bearer " + this.jwt);
        }

        this.httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(API_ENDPOINT + path))
                .timeout(Duration.ofSeconds(this.TIMEOUT))
                .setHeader("Content-Type", "application/json")
                .headers(header.toArray(new String[0]))
                .POST(HttpRequest.BodyPublishers.ofString(payload))
                .build();

        HttpResponse<String> httpResponse;

        try {
            httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            System.out.println("Failed to connect");
            throw new RuntimeException(e);
        }

        if (httpResponse.statusCode() != 200) {
            System.out.println("Error connecting to API");

            return null;

        }

        return httpResponse.body();
    }

    /**
     * Sends user credentials to API for authentication and saves the
     * JWT for later queries
     *
     * @throws JsonProcessingException error during JSON conversion
     */
    private void authenticate() throws JsonProcessingException {

        ObjectNode request = this.mapper.createObjectNode();
        ObjectNode response;

        request.put("email", this.email);
        request.put("password", this.password);

        response = this.mapper.readValue(this.query("login", request.toString()), ObjectNode.class);


        if (response.get("success").asBoolean()) {
            System.out.println("Connected");
        } else {
            System.out.println("Wrong e-mail or password");
        }

        this.jwt = response.get("token").asText();
    }

    /**
     * Checks if JWT is still valid
     *
     * @return Returns true if JWT is valid / false if not
     */
    private boolean ok() {

        if (this.jwt != null) {

            DecodedJWT token = JWT.decode(this.jwt);

            if (token.getExpiresAt().after(new Date())) {
                return true;
            } else {
                this.jwt = null;
            }
        }

        return false;
    }

    /**
     * List all installations (Address with one or multiple electricity meters)
     *
     * @return an Arraylist of Installations
     * @throws JsonProcessingException error during JSON conversion
     */
    public ArrayList<Installation> listInstallations() throws JsonProcessingException {

        if (!ok()) {
            this.authenticate();
        }

        ArrayList<Installation> response;

        response = this.mapper.readValue(this.query("getInstallations", ""), this.typeFactory.constructCollectionType(ArrayList.class, Installation.class));

        for (Installation i : response) {
            i.setStromNetz(this);
        }

        return response;
    }

    /**
     * Lists consumption data from startDate to stopDate
     *
     * @param meter     electricity meter to fetch the data from
     * @param startDate startDate
     * @param stopDate  stopDate
     * @return an ArrayList containing all readings
     * @throws JsonProcessingException error during JSON conversion
     */
    public ArrayList<Reading> fetch_consumption_data(Meter meter, Date startDate, Date stopDate) throws JsonProcessingException {
        ObjectNode request = this.mapper.createObjectNode();
        String interval;

        if (meter.getOptState().getCurrentOptState().equals("OptMiddle")) {
            interval = "Daily";
        } else if (meter.getOptState().getCurrentOptState().equals("OptIn")) {
            interval = "QuarterHourly";
        } else {
            System.out.println("Meter is neither IME nor IMS, no data");

            return null;
        }

        request.put("unitOfConsump", "KWH");
        request.put("interval", interval);
        request.put("meterPointId", meter.getMeterPointID());
        request.put("fromDate", startDate.getTime());
        request.put("toDate", stopDate.getTime());

        ArrayList<Reading> readings;

        readings = this.mapper.convertValue(this.mapper.readValue(this.query("getMeterReading", request.toString()), ObjectNode.class).get("readings"),
                this.typeFactory.constructCollectionType(ArrayList.class, Reading.class));

        return readings;
    }

    /**
     * Lists consumption data from now - days
     *
     * @param meter electricity meter to fetch the data from
     * @param days  days in the past from which you need to get data
     * @return an ArrayList containing all readings
     * @throws JsonProcessingException error during JSON conversion
     */
    public ArrayList<Reading> fetch_consumption_data(Meter meter, int days) throws JsonProcessingException {
        Date startDate = new Date();
        Date xDaysAgo = new Date(startDate.getTime() - ((long) days * 24 * 60 * 60 * 1000));

        return this.fetch_consumption_data(meter, startDate, xDaysAgo);
    }

    /**
     * Lists all consumption data available
     *
     * @param meter electricity meter to fetch the data from
     * @return an ArrayList containing all readings
     * @throws JsonProcessingException error during JSON conversion
     */
    public ArrayList<Reading> fetch_consumption_data(Meter meter) throws JsonProcessingException {
        Date startDate = meter.getReadingsAvailableSince();
        Date stopDate = new Date();

        return this.fetch_consumption_data(meter, startDate, stopDate);
    }
}

