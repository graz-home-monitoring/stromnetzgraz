package org.sngraz.reading;

public class ReadingValues {

    private String scale;
    private String readingType;
    private String unit;
    private String readingState;
    private float value;

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public String getReadingType() {
        return readingType;
    }

    public void setReadingType(String readingType) {
        this.readingType = readingType;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getReadingState() {
        return readingState;
    }

    public void setReadingState(String readingState) {
        this.readingState = readingState;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ReadingValues{" +
                "scale='" + scale + '\'' +
                ", readingType='" + readingType + '\'' +
                ", unit='" + unit + '\'' +
                ", readingState='" + readingState + '\'' +
                ", value=" + value +
                '}';
    }
}
