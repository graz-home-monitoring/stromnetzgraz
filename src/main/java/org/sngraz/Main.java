package org.sngraz;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws JsonProcessingException {

        var email = "";
        var password = "";

        StromNetz sn = new StromNetz(email, password);
        ArrayList<Installation> installations = sn.listInstallations();


        for (Installation i : installations) {
            for (Meter m : i.getMeterPoints()) {
                System.out.println(sn.fetch_consumption_data(m));

            }
        }
    }

}