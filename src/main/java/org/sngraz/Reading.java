package org.sngraz;

import org.sngraz.reading.ReadingValues;

import java.util.ArrayList;
import java.util.Date;

public class Reading {

    private Date readTime;
    private ArrayList<ReadingValues> readingValues;

    public Date getReadTime() {
        return readTime;
    }

    public void setReadTime(Date readTime) {
        this.readTime = readTime;
    }

    public ArrayList<ReadingValues> getReadingValues() {
        return readingValues;
    }

    public void setReadingValues(ArrayList<ReadingValues> readingValues) {
        this.readingValues = readingValues;
    }

    @Override
    public String toString() {
        return "Reading {" + "\n" +
                "readTime=" + readTime + "\n" +
                ", readingValues=" + readingValues +
                '}';
    }
}
