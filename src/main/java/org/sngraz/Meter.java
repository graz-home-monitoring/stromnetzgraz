package org.sngraz;

import org.sngraz.meter.AccountingPeriode;
import org.sngraz.meter.CustomerInterfaceState;
import org.sngraz.meter.DisplayState;
import org.sngraz.meter.OptState;

import java.util.Date;

public class Meter {

    private int meterPointID;
    private CustomerInterfaceState customerInterfaceState;
    private DisplayState displayState;
    private String name;
    private OptState optState;
    private AccountingPeriode accountingPeriode;


    private String shortName;
    private boolean singleInvoiceCustomer;
    private Date readingsAvailableSince;

    public int getMeterPointID() {
        return meterPointID;
    }

    public void setMeterPointID(int meterPointID) {
        this.meterPointID = meterPointID;
    }

    public CustomerInterfaceState getCustomerInterfaceState() {
        return customerInterfaceState;
    }

    public void setCustomerInterfaceState(CustomerInterfaceState customerInterfaceState) {
        this.customerInterfaceState = customerInterfaceState;
    }

    public DisplayState getDisplayState() {
        return displayState;
    }

    public void setDisplayState(DisplayState displayState) {
        this.displayState = displayState;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OptState getOptState() {
        return optState;
    }

    public void setOptState(OptState optState) {
        this.optState = optState;
    }

    public AccountingPeriode getAccountingPeriode() {
        return accountingPeriode;
    }

    public void setAccountingPeriode(AccountingPeriode accountingPeriode) {
        this.accountingPeriode = accountingPeriode;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public boolean isSingleInvoiceCustomer() {
        return singleInvoiceCustomer;
    }

    public void setSingleInvoiceCustomer(boolean singleInvoiceCustomer) {
        this.singleInvoiceCustomer = singleInvoiceCustomer;
    }

    public Date getReadingsAvailableSince() {
        return readingsAvailableSince;
    }

    public void setReadingsAvailableSince(Date readingsAvailableSince) {
        this.readingsAvailableSince = readingsAvailableSince;
    }

    @Override
    public String toString() {
        return "Meter{" +
                "meterPointID=" + meterPointID +
                ", customerInterfaceState=" + customerInterfaceState +
                ", displayState=" + displayState +
                ", name='" + name + '\'' +
                ", optState=" + optState +
                ", accountingPeriode=" + accountingPeriode +
                ", shortName='" + shortName + '\'' +
                ", singleInvoiceCustomer=" + singleInvoiceCustomer +
                ", readingsAvailableSince=" + readingsAvailableSince +
                '}';
    }
}


