package org.sngraz;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;

public class Installation {

    @JsonIgnore
    private StromNetz stromNetz;
    private String deliveryDirection;
    private String address;
    private int installationNumber;
    private ArrayList<Meter> meterPoints;
    private int customerID;
    private int installationID;
    private int customerNumber;


    public String getDeliveryDirection() {
        return deliveryDirection;
    }

    public void setDeliveryDirection(String deliveryDirection) {
        this.deliveryDirection = deliveryDirection;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getInstallationNumber() {
        return installationNumber;
    }

    public void setInstallationNumber(int installationNumber) {
        this.installationNumber = installationNumber;
    }

    public ArrayList<Meter> getMeterPoints() {
        return meterPoints;
    }

    public void setMeterPoints(ArrayList<Meter> meterPoints) {
        this.meterPoints = meterPoints;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getInstallationID() {
        return installationID;
    }

    public void setInstallationID(int installationID) {
        this.installationID = installationID;
    }

    public int getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    public StromNetz getStromNetz() {
        return stromNetz;
    }

    public void setStromNetz(StromNetz stromNetz) {
        this.stromNetz = stromNetz;
    }

    @Override
    public String toString() {
        return "Installation{" +
                "deliveryDirection='" + deliveryDirection + '\'' +
                ", address='" + address + '\'' +
                ", installationNumber=" + installationNumber +
                ", meterPoints=" + meterPoints +
                ", customerID=" + customerID +
                ", installationID=" + installationID +
                ", customerNumber=" + customerNumber +
                '}';
    }
}