package org.sngraz.meter;

import java.util.ArrayList;

public class AccountingPeriode {

    private String currentAccountingPeriode;
    private ArrayList<AccountingPeriodeOptions> accountingPeriodeOptions;

    public String getCurrentAccountingPeriode() {
        return currentAccountingPeriode;
    }

    public void setCurrentAccountingPeriode(String currentAccountingPeriode) {
        this.currentAccountingPeriode = currentAccountingPeriode;
    }

    public ArrayList<AccountingPeriodeOptions> getAccountingPeriodeOptions() {
        return accountingPeriodeOptions;
    }

    public void setAccountingPeriodeOptions(ArrayList<AccountingPeriodeOptions> accountingPeriodeOptions) {
        this.accountingPeriodeOptions = accountingPeriodeOptions;
    }

    @Override
    public String toString() {
        return "AccountingPeriode{" +
                "currentAccountingPeriode='" + currentAccountingPeriode + '\'' +
                ", accountingPeriodeOptions=" + accountingPeriodeOptions +
                '}';
    }
}