package org.sngraz.meter;

public class AccountingPeriodeOptions {

    private String accountingPeriode;
    private boolean applicable;

    public String getAccountingPeriode() {
        return accountingPeriode;
    }

    public void setAccountingPeriode(String accountingPeriode) {
        this.accountingPeriode = accountingPeriode;
    }

    public boolean isApplicable() {
        return applicable;
    }

    public void setApplicable(boolean applicable) {
        this.applicable = applicable;
    }
}