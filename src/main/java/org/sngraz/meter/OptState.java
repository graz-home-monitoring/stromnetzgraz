package org.sngraz.meter;

import java.util.ArrayList;

public class OptState {

    private String currentOptState;
    private ArrayList<OptStateOptions> optStateOptions;

    public String getCurrentOptState() {
        return currentOptState;
    }

    public void setCurrentOptState(String currentOptState) {
        this.currentOptState = currentOptState;
    }

    public ArrayList<OptStateOptions> getOptStateOptions() {
        return optStateOptions;
    }

    public void setOptStateOptions(ArrayList<OptStateOptions> optStateOptions) {
        this.optStateOptions = optStateOptions;
    }

    @Override
    public String toString() {
        return "OptState{" +
                "currentOptState='" + currentOptState + '\'' +
                ", optStateOptions=" + optStateOptions +
                '}';
    }
}