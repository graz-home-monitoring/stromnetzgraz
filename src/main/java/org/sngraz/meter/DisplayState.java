package org.sngraz.meter;

import java.util.ArrayList;

public class DisplayState {

    private String currentDisplayState;
    private ArrayList<DisplayStateOptions> displayStateOptions;

    public String getCurrentDisplayState() {
        return currentDisplayState;
    }

    public void setCurrentDisplayState(String currentDisplayState) {
        this.currentDisplayState = currentDisplayState;
    }

    public ArrayList<DisplayStateOptions> getDisplayStateOptions() {
        return displayStateOptions;
    }

    public void setDisplayStateOptions(ArrayList<DisplayStateOptions> displayStateOptions) {
        this.displayStateOptions = displayStateOptions;
    }

    @Override
    public String toString() {
        return "DisplayState{" +
                "currentDisplayState='" + currentDisplayState + '\'' +
                ", displayStateOptions=" + displayStateOptions +
                '}';
    }
}