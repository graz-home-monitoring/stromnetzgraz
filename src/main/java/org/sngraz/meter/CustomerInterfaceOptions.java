package org.sngraz.meter;

public class CustomerInterfaceOptions {

    private String customterInterfaceState;
    private boolean applicable;

    public String getCustomterInterfaceState() {
        return customterInterfaceState;
    }

    public void setCustomterInterfaceState(String customterInterfaceState) {
        this.customterInterfaceState = customterInterfaceState;
    }

    public boolean isApplicable() {
        return applicable;
    }

    public void setApplicable(boolean applicable) {
        this.applicable = applicable;
    }
}