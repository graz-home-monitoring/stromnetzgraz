package org.sngraz.meter;

import java.util.ArrayList;

public class CustomerInterfaceState {

    public String currentCustomerInterfaceState;
    public ArrayList<CustomerInterfaceOptions> customerInterfaceOptions;


    public String getCurrentCustomerInterfaceState() {
        return currentCustomerInterfaceState;
    }

    public void setCurrentCustomerInterfaceState(String currentCustomerInterfaceState) {
        this.currentCustomerInterfaceState = currentCustomerInterfaceState;
    }

    public ArrayList<CustomerInterfaceOptions> getCustomerInterfaceOptions() {
        return customerInterfaceOptions;
    }

    public void setCustomerInterfaceOptions(ArrayList<CustomerInterfaceOptions> customerInterfaceOptions) {
        this.customerInterfaceOptions = customerInterfaceOptions;
    }

    @Override
    public String toString() {
        return "CustomerInterfaceState{" +
                "currentCustomerInterfaceState='" + currentCustomerInterfaceState + '\'' +
                ", customerInterfaceOptions=" + customerInterfaceOptions +
                '}';
    }
}