package org.sngraz.meter;

public class OptStateOptions {

    private String optState;
    private boolean applicable;


    public String getOptState() {
        return optState;
    }

    public void setOptState(String optState) {
        this.optState = optState;
    }

    public boolean isApplicable() {
        return applicable;
    }

    public void setApplicable(boolean applicable) {
        this.applicable = applicable;
    }
}